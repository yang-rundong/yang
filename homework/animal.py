class Animal:
    name = ""
    color = ""
    age = 0
    gender = ""

    def __init__(self,n,c,a,g):
        self.name = n
        self.color = c
        self.age = a
        self.gender = g

    def call(self):
        print("会叫")

    def run(self):
        print("会跑")
import animal

class Cat(animal.Animal):
    hair = "短毛"

    def skill(self):
        print("捉到老鼠了哦~\n")

    def __init__(self,n,c,a,g,h):
        animal.Animal.__init__(self, n, c, a, g)
        self.hair = h

    def call(self):
        print("喵喵~")

    def information(self):
        print("这是只可爱的小猫",end=' ')
        self.call()
        print("名字叫：%s\n"
              "颜色是：%s\n"
              "年龄是：%d岁\n"
              "毛发为：%s"
              %(self.name,self.color,self.age,self.hair))
        self.skill()
